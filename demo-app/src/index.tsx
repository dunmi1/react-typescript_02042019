import React, { Suspense } from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';

import { Color } from './color-tool';
import { Car } from './car-tool';

import './index.css';

import {
  Header as PageHeader,
  Footer as PageFooter,
  Menu as DashboardMenu,
} from './dashboard';

import { HomeRoute, ColorToolRoute, CarToolRoute } from './routes';
import { GridLayout, GridArea } from './shared/components/Grid';

const colorList: Color[] = [
  { id: 1, name: 'steel blue', hexCode: '#4682B4' },
  { id: 2, name: 'chartreuse', hexCode: '#7fff00' },
  { id: 3, name: 'blue', hexCode: '#0000FF' },
];

const carList: Car[] = [
  { id: 1, make: 'Ford', model: 'F-150', year: 2016, color: 'blue', price: 12000 },
  { id: 2, make: 'Ford', model: 'F-250', year: 2012, color: 'red', price: 14000 },
];

const toolAppName = 'The Amazing Tool App';

ReactDOM.render(
  <Router>
    <GridLayout gridClass="page-layout">
      <GridArea gridArea="page-header">
        <PageHeader pageHeader={toolAppName} />
      </GridArea>
      <GridArea gridArea="menu">
        <DashboardMenu />
      </GridArea>
      <GridArea gridArea="content" className="content">
        <Suspense fallback={'Loading...'}>
          <Switch>
            <Route path="/color-tool" render={() => <ColorToolRoute colors={colorList} />} />
            <Route path="/car-tool" render={() => <CarToolRoute cars={carList} />} />
            <Route path="/" exact render={() => <HomeRoute />} />
          </Switch>
        </Suspense>
      </GridArea>
      <GridArea gridArea="page-footer">
        <PageFooter />
      </GridArea>
    </GridLayout>
  </Router>,
  document.querySelector('#root'),
);
