import React from 'react';

import { Car } from '../models/Car';

import { CarViewRow } from './CarViewRow';
import { CarEditRow } from './CarEditRow';

import './CarTable.css';

interface CarTableProps {
  cars: Car[];
  editCarId: number;
  onEditCar: (carId: number) => void;
  onDeleteCar: (carId: number) => void;
  onSaveCar: (car: Car) => void;
  onCancelCar: () => void;
}

export const CarTable = (props: CarTableProps) => {

  const {
    cars, editCarId, onEditCar,
    onDeleteCar, onSaveCar, onCancelCar,
  } = props;

  return <table className='CarTable'>
    <thead>
      <tr>
        <th>Id</th>
        <th>Make</th>
        <th>Model</th>
        <th>Year</th>
        <th>Color</th>
        <th>Price</th>
        <th>Actions</th>
      </tr>
    </thead>
    <tbody>
      {cars.map(car => car.id === editCarId
        ? <CarEditRow key={car.id} car={car} onSaveCar={onSaveCar} onCancelCar={onCancelCar} />
        : <CarViewRow key={car.id} car={car} onDeleteCar={onDeleteCar} onEditCar={onEditCar} />)}
    </tbody>
  </table>;

};


// (boolean_expr) ? funcA() : funcB()