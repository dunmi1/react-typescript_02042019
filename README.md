# Welcome to React with TypeScript Class!

## Instructor

Eric Greene - [http://t4d.io](http://t4d.io)

## Schedule

Class:

- Monday through Friday: 1pm to 4:30pm

Breaks:

- Afternoon Break: 2:40pm to 2:50pm

## Course Outline

- Day 1 - What is React, Functional Components, JSX, Props, Default Props
- Day 2 - Class Components, State, State Hook, Reducer Hook
- Day 3 - Composition (including containment + specialization)
- Day 4 - Composition (including containment + specialization)
- Day 5 - Lifecycle Functions (including Effect Hook), Keys, Refs (including Ref Hook)

## Links

### Instructor's Resources

- [Accelebrate, Inc.](https://www.accelebrate.com/)
- [WintellectNOW](https://www.wintellectnow.com/Home/Instructor?instructorId=EricGreene) - Special Offer Code: GREENE-2016

### Other Resources

- [You Don't Know JS](https://github.com/getify/You-Dont-Know-JS)
- [JavaScript Air Podcast](http://javascriptair.podbean.com/)
- [Speaking JavaScript](http://speakingjs.com/es5/)

## Useful Resources

- [React](https://reactjs.org/)
- [Redux](https://redux.js.org/)
- [TypeScript](https://www.typescriptlang.org/)