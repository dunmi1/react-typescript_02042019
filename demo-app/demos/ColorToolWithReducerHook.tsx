import React, { useReducer } from 'react';

import { Color } from '../models/Color';

interface Action {
  type: string;
  [ x: string ]: any;
}

interface State {
  name: string;
  hexCode: string;
  colors: Color[];
}

const reducer = (state: State, action: Action) => {
  switch (action.type) {
    case 'entry':
      return {
        ...state,
        [ action.name ]: action.value,
      };
    case 'reset_form':
      return {
        ...state,
        name: '',
        hexCode: '',
      };
    case 'add_color':
      return {
        ...state,
        colors: state.colors.concat({
          ...action.color,
          id: Math.max(...state.colors.map((c: Color) => c.id) as number[], 0) + 1
        }),
      };
    default:
      return state;
    }
};


interface ColorToolProps {
  colors: Color[];
}

export const ColorTool = (props: ColorToolProps) => {

  const [ state, dispatch ] = useReducer(reducer, {
    name: '', hexCode: '', colors: props.colors.concat(),
  });

  const addColor = () => {
    dispatch({ type: 'add_color', color: { name: state.name, hexCode: state.hexCode } });
    dispatch({ type: 'reset_form' });
  };

  const change = (e: React.ChangeEvent<HTMLInputElement>) => {
    dispatch({ type: 'entry', value: e.target.value, name: e.target.name });
  };

  return <>
    <header>
      <h1>Color Tool</h1>
    </header>
    <ul>
      {state.colors.map((color: Color) => <li key={color.id}>{color.name} - {color.hexCode}</li>)}
    </ul>
    <form>
      <div>
        <label htmlFor="name-input">Color Name:</label>
        <input type="text" id="name-input" name="name"
          value={state.name} onChange={change} />
      </div>
      <div>
        <label htmlFor="hexcode-input">Color HexCode:</label>
        <input type="text" id="hexcode-input" name="hexCode"
          value={state.hexCode} onChange={change} />
      </div>
      <button type="button" onClick={addColor}>Add Color</button>
    </form>
  </>;
};