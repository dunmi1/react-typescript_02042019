import React, { useState } from 'react';
import { omit } from 'lodash';

import { Car } from '../models/Car';

interface CarEditRowProps {
  car: Car;
  onSaveCar: (car: Car) => void;
  onCancelCar: () => void;
}

export const CarEditRow = ({ car, onSaveCar, onCancelCar }: CarEditRowProps) => {

  const [ carForm, setCarForm ] = useState(omit(car, ['id']));

  return <tr>
    <td>{car.id}</td>
    <td><input type="text" value={carForm.make}
      onChange={e => setCarForm({ ...carForm, make: e.target.value })} /></td>
    <td><input type="text" value={carForm.model}
      onChange={e => setCarForm({ ...carForm, model: e.target.value })} /></td>
    <td><input type="text" value={carForm.year}
      onChange={e => setCarForm({ ...carForm, year: Number(e.target.value) })} /></td>
    <td><input type="text" value={carForm.color}
      onChange={e => setCarForm({ ...carForm, color: e.target.value })} /></td>
    <td><input type="text" value={carForm.price}
      onChange={e => setCarForm({ ...carForm, price: Number(e.target.value) })} /></td>
    <td>
      <button type="button" onClick={() => onSaveCar({ ...carForm, id: car.id })}>Save</button>
      <button type="button" onClick={onCancelCar}>Cancel</button>
    </td>
  </tr>;
};