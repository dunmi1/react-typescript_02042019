import React from 'react';

import { Car } from '../models/Car';

interface CarViewRowProps { 
  car: Car;
  onDeleteCar: (carId: number) => void;
  onEditCar: (carId: number) => void;
}

export const CarViewRow = ({
  car, onDeleteCar, onEditCar
}: CarViewRowProps) => {

  return <tr>
    <td>{car.id}</td>
    <td>{car.make}</td>
    <td>{car.model}</td>
    <td>{car.year}</td>
    <td>{car.color}</td>
    <td>{car.price}</td>
    <td>
      <button type="button"
        onClick={() => onEditCar(car.id as number)}>Edit</button>
      <button type="button"
        onClick={() => onDeleteCar(car.id as number)}>Delete</button>
    </td>
  </tr>;
};