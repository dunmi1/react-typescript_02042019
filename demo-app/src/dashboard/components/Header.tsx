import React from 'react';

import './Header.css';

export const Header = ({ pageHeader }: { pageHeader?: string }) =>
  <header className="Header">
    <h1>{pageHeader}</h1>
  </header>;


Header.defaultProps = {
  pageHeader: 'Tool Apps',
};
