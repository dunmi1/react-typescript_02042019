Exercise 2

https://gitlab.com/t4d-classes/react-typescript_02042019

1. Create an array of two cars. Each car should be an object (do not use a class to construct the object) and the two cars should be stored in an array. It will be an array of car objects.

const cars = [
  { id: 1, make: 'Ford', model: 'F-150', year: 2016, color: 'blue', price: 12000 },
  { id: 2, make: 'Ford', model: 'F-250', year: 2012, color: 'red', price: 14000 },
];

Each car should have the following fields:

id: number
make: string
model: string
year: number
color: string
price: number

2. Display the car array in an HTML table. Please use a table tag and its related tags. Be sure to display a column header for each column. If you are not familiar with the table element what should you do? Google it!

3. Check the console to ensure there are no errors.

4. Ensure the cars are displayed in the table.