import React from 'react';
import { NavLink } from 'react-router-dom';

import './Menu.css';

export const Menu = () => {
  return <nav className="Menu">
    <ul>
      <li><NavLink to="/" activeClassName="active" exact>Home</NavLink></li>
      <li><NavLink to="/color-tool" activeClassName="active">Color Tool</NavLink></li>
      <li><NavLink to="/car-tool" activeClassName="active">Car Tool</NavLink></li>
    </ul>
  </nav>;
};