import React from 'react';

import './Footer.css';

export const Footer = ({ orgName } : { orgName?: string }) =>
  <footer>
    <small>&copy; {new Date().getFullYear()} {orgName}</small>
  </footer>;

Footer.defaultProps = {
  orgName: 'Sample Company, Inc.',
};