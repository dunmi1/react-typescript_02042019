import React from 'react';

import { Color } from '../models/Color';

interface ColorListProps {
  colors: Color[];
  onDeleteColor: (colorId: number | undefined) => void;
}

export const ColorList = ({ colors, onDeleteColor }: ColorListProps) => {

  return <ul>
    {colors.map(color => <li key={color.id}>
      {color.name} - {color.hexCode}
      <button type="button" onClick={() => onDeleteColor(color.id)}>Delete</button>
    </li>)}
  </ul>;

};

