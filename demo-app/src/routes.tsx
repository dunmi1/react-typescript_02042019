import { lazy } from 'react';

export const HomeRoute = lazy(() => import('./dashboard/components/Home'));
export const ColorToolRoute = lazy(() => import('./color-tool/components/ColorTool'));
export const CarToolRoute = lazy(() => import('./car-tool/components/CarTool'));
